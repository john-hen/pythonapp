﻿# Imports here define the public API of the package.

from .main import run
from .meta import version as __version__
from .meta import synopsis as __doc__
