﻿"""Meta-information about the application."""

title     = 'PythonApp'
synopsis  = 'Demo project for CI build pipeline.'
version   = '0.9.5'
author    = 'John Hennig'
