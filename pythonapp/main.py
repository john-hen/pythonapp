﻿"""Main application."""

from . import meta
from yaml import safe_load
from rich import print
from pathlib import Path


def load() -> dict:
    """Loads the configuration"""
    here = Path(__file__).parent
    file = here/'config.yaml'
    content = file.read_text(encoding='UTF-8')
    config = safe_load(content)
    return config


def run():
    """Runs the application."""
    print(f'{meta.title} {meta.version}')
    config = load()
    print(config)
