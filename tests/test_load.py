﻿"""Tests the `load` function."""

from pythonapp import main


def test_load():
    config = main.load()
    assert 'section1' in config
    assert 'section2' in config
