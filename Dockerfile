FROM python:latest

ARG INDEX_URL

RUN pip install PythonApp --index-url $INDEX_URL

ENTRYPOINT ["pythonapp"]
