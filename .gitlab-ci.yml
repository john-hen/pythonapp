# Default Docker image. Used by jobs that don't specify otherwise.
# Find available images on hub.docker.com. For added robustness,
# use a specific image tag instead of "latest".
image: python:latest

# Define pipeline stages. They will run in the order defined here, if at all.
stages:
  - test
  - publish
  - containerize


# Test the package. Runs on every commit / merge request.
test:
  stage: test
  variables:
    PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip
  cache:
    paths:
      - .cache/pip
  script:
    - echo "Installing package and development dependencies."
    - python --version
    - pip install --editable .[dev]
    - echo "Running code linter."
    - flake8
    - echo "Running test suite and measuring code coverage."
    - pytest --cov
    - echo "Testing wheel build."
    - flit build --format wheel

# We have configured Pip's cache directory to be inside the project folder
# by setting the PIP_CACHE_DIR variable above. This is so that we can tell
# the GitLab runner to cache its content, i.e. the Python dependencies,
# and make them available in subsequent jobs and pipeline runs. The `cache`
# keyword only accepts paths that are in the project directory.
# https://docs.gitlab.com/ee/ci/yaml/index.html#cache
#
# Furthermore, we have installed the package in "editable" mode. This is
# so the code in the project folder is executed. With a regular install,
# it would end up in the Python directory and code coverage would be
# reported as zero.


# Publish the package to this project's own "PyPI" registry.
publish:
  stage: publish
  variables:
    PIP_CACHE_DIR:  $CI_PROJECT_DIR/.cache/pip
    PACKAGE_INDEX:  $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/pypi
    FLIT_INDEX_URL: $PACKAGE_INDEX
    FLIT_USERNAME:  gitlab-ci-token
    FLIT_PASSWORD:  $CI_JOB_TOKEN
  cache:
    paths:
      - .cache/pip
  script:
    - echo "Installing package and development dependencies."
    - pip install .[dev]
    - echo "Publishing to package registry at $PACKAGE_INDEX."
    - flit publish --format wheel
  rules:
    - if: $CI_COMMIT_TAG


# Build a Docker image with the package pre-installed.
containerize:
  stage: containerize
  image: docker:latest
  services:
    - docker:dind
  variables:
    PACKAGE_INDEX: $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/pypi
    INDEX_URL:     $PACKAGE_INDEX/simple
    VERSION_TAG:   $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    LATEST_TAG:    $CI_REGISTRY_IMAGE:latest
  script:
    - echo "Logging into container registry."
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - echo "Pulling latest image to enable Docker layer caching."
    - docker pull $LATEST_TAG || true
    - echo "Building the Docker image."
    - docker build
      --cache-from $LATEST_TAG
      --tag $VERSION_TAG
      --tag $LATEST_TAG
      --build-arg INDEX_URL=$INDEX_URL .
    - echo "Publishing the Docker image."
    - docker push $VERSION_TAG
    - docker push $LATEST_TAG
  rules:
    - if: $CI_COMMIT_TAG

# We use the "Docker-in-Docker" (dind) service to build new images.
# https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker
# An alterative would be to use Google's container tool Kaniko.
# https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
# We use the previous Docker image for layer caching to speed up the build.
# https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#make-docker-in-docker-builds-faster-with-docker-layer-caching
# Note that this means you shouldn't manually rerun pipelines for older tags,
# as that would overwrite the latest image.
